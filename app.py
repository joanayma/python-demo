from datetime import import datetime

def application(env, start_response):
    start_response('200 OK', [('Content-Type', 'text/html')])
    return [datetime.utcnow().strftime('%M/%d/%Y %H:%M:%S %P')]
